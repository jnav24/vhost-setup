<?php

	class Vhost
	{
		public $confPath;
		public $currentPath = __DIR__;
		private $filename;
		private $url = array(
			'.com',
			'.org',
			'.net',
			'.dev'
		);

		public function createNewDomain($domain, $path, $confPath)
		{
			$this->confPath = $confPath;
			$conf = $this->getConf($domain, $path);
			$this->createConfFile($domain, $conf);
			$this->moveFileToApache();
			$this->setApache();
		}

		private function getConf($domain, $path)
		{
			$conf = "<VirtualHost *:80>
			    # The ServerName directive sets the request scheme, hostname and port that
			    # the server uses to identify itself. This is used when creating
			    # redirection URLs. In the context of virtual hosts, the ServerName
			    # specifies what hostname must appear in the request's Host: header to
			    # match this virtual host. For the default virtual host (this file) this
			    # value is not decisive as it is used as a last resort host regardless.
			    # However, you must set it for any further virtual host explicitly.
			    #ServerName www.example.com

			    ServerAdmin jn@localhost
			    ServerName $domain
			    ServerAlias www.{$domain}
			    DocumentRoot $path

			    # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
			    # error, crit, alert, emerg.
			    # It is also possible to configure the loglevel for particular
			    # modules, e.g.
			    #LogLevel info ssl:warn

			    ErrorLog \${APACHE_LOG_DIR}/error.log
			    CustomLog \${APACHE_LOG_DIR}/access.log combined

			    # For most configuration files from conf-available/, which are
			    # enabled or disabled at a global level, it is possible to
			    # include a line for only one particular virtual host. For example the
			    # following line enables the CGI configuration for this host only
			    # after it has been globally disabled with \"a2disconf\".
			    #Include conf-available/serve-cgi-bin.conf

			    <Directory $path>
			        Options Indexes FollowSymLinks
			        AllowOverride All
			        RewriteEngine On
			    </Directory>
			</VirtualHost>";

			$conf = preg_replace('/\t+/', "\t", $conf);
			return $conf;
		}

		private function createConfFile($domain, $conf)
		{
			$this->filename = str_replace($this->url, '.conf', $domain);
			$fp = fopen($this->currentPath . "/" . $this->filename, "wb");
			fwrite($fp, $conf);
			fclose($fp);
		}

		private function moveFileToApache()
		{
			exec("sudo mv " . $this->currentPath . "/" . $this->filename . " " . $this->confPath);
			exec("sudo chown root:root " . $this->confPath . $this->filename);
		}

		private function setApache()
		{
			exec("cd " . $this->confPath);
			exec("sudo a2ensite " . $this->filename);
			exec("sudo service apache2 restart");
			exec("cd " . $this->currentPath);
		}
	}

	if(!isset($argv[1]))
	{
		die('Please enter a domain.' . "\n");
	}

	if(!isset($argv[2]))
	{
		die('Please enter a path.' . "\n");
	}

	$confPath = isset($argv[3]) ? $argv[3] : "/etc/apache2/sites-available/";

	$domain = new Vhost();
	$domain->createNewDomain($argv[1],$argv[2], $confPath);