<?php

	class Symbolic
	{
		private $sync;

		public function createNewSite($file)
		{
			$file = $this->getPathAndFile($file);

			if(file_exists('/var/www/' . $this->sync))
			{
				die($this->sync . " already exists.\n");
			}
			
			exec('sudo ln -s ' . $file . ' /var/www/' . $this->sync);
			die($this->sync . ' has been created.' . "\n");
		}

		private function getPathAndFile($file)
		{
			$file = rtrim($file,'/');
			$path = explode('/', $file);

			if(count($path) > 1)
			{
				$this->sync = end($path);
				return $file;
			}

			$this->sync = $file;
			return __DIR__ . '/' . $file;
		}
	}

	if(!isset($argv[1]))
	{
		die('Please enter a domain.' . "\n");
	}

	$domain = new Symbolic();
	$domain->createNewSite($argv[1]);