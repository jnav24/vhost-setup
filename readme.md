This is to create a new symbolic link in the `/var/www` and create a new vhost. The code works in a linux enviroment and assumes you hvae a `/var/www' directory.


# Instructions

## symbolic link

```
	php symbolic.php /path/to/site
```

## vhost

```
	php vhost.php newsite.dev /path/to/site
```